Content-Type: multipart/mixed; boundary="==BOUNDARY=="
MIME-Version: 1.0

--==BOUNDARY==
Content-Type: text/cloud-config; charset="us-ascii"
#cloudconfig
packages:
  - jq
  - awslogs
  - nfs-utils
  - python27
  - python27-pip

--==BOUNDARY==
Content-Type: text/x-shellscript; charset="us-ascii"
#!/bin/bash -ex
exec > /tmp/upgrade-aws-cli.log 2>&1
PATH=$PATH:/usr/local/bin
pip install --upgrade awscli

--==BOUNDARY==
Content-Type: text/x-shellscript; charset="us-ascii"
#!/bin/bash -ex
exec > /tmp/set-cluster-name.log 2>&1
echo "ECS_CLUSTER=mtv-stage" >> /etc/ecs/ecs.config
--==BOUNDARY==
Content-Type: text/x-shellscript; charset="us-ascii"
#!/bin/bash -ex
exec > /tmp/install-opsworks-agent.log 2>&1
#register this machine with opsworks stack so we can ssh into it
EC2_REGION=$(curl -s 169.254.169.254/latest/dynamic/instance-identity/document | jq -r .region)

OPS_WORK_STACK=ne.stage
OPS_WORK_STACK_ID=`aws opsworks describe-stacks --region "$EC2_REGION" | jq -r --arg OPS_WORK_STACK "$OPS_WORK_STACK" '.Stacks[] | select(.Name==$OPS_WORK_STACK) | .StackId'`
aws opsworks register --infrastructure-class ec2 --region "$EC2_REGION" --stack-id "$OPS_WORK_STACK_ID" --local --use-instance-profile
--==BOUNDARY==
Content-Type: text/x-shellscript; charset="us-ascii"
#!/bin/bash -ex
exec > /tmp/mnt-efs.log 2>&1
EFS_NAME=public-stage-wordpress-content
DIR_TGT=/mnt/efs
PATH=$PATH:/usr/local/bin

#Get region of EC2 from instance metadata
EC2_AVAIL_ZONE=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`
EC2_REGION="`echo \"$EC2_AVAIL_ZONE\" | sed -e 's:\([0-9][0-9]*\)[a-z]*\$:\\1:'`"

#Add support for EFS to the CLI configuration
aws configure set preview.efs true
#Create mount point
mkdir /mnt/efs
#Get EFS FileSystemID attribute
#Instance needs to be added to a EC2 role that give the instance at least read access to EFS
EFS_FILE_SYSTEM_ID=`aws efs describe-file-systems --region "$EC2_REGION" |  jq '.FileSystems[]' | jq -r --arg EFS_NAME "$EFS_NAME" 'select(.Name==$EFS_NAME)' | jq -r '.FileSystemId'`
#Check to see if the variable is set. If not, then exit.
if [ -z "$EFS_FILE_SYSTEM_ID" ]; then
  echo "ERROR: variable not set" 1> /etc/efssetup.log
  exit
fi
#Instance needs to be a member of security group that allows 2049 inbound/outbound
#The security group that the instance belongs to has to be added to EFS file system configuration
#Create variables for source and target
DIR_SRC=$EC2_AVAIL_ZONE.$EFS_FILE_SYSTEM_ID.efs.$EC2_REGION.amazonaws.com
DIR_TGT=/mnt/efs
#Mount EFS file system
mount -t nfs4 $DIR_SRC:/ $DIR_TGT
#Backup fstab
cp -p /etc/fstab /etc/fstab.back-$(date +%F)
#Append line to fstab
echo -e "$DIR_SRC:/ \t\t $DIR_TGT \t\t nfs \t\t defaults \t\t 0 \t\t 0" | tee -a /etc/fstab
--==BOUNDARY==
Content-Type: text/x-shellscript; charset="us-ascii"
#!/bin/bash -ex
exec > /tmp/template-for-cloudwatch-agent.log 2>&1
# Inject the CloudWatch Logs configuration file contents
cat > /etc/awslogs/awslogs.conf <<- EOF
[general]
state_file = /var/lib/awslogs/agent-state

[/var/log/dmesg]
file = /var/log/dmesg
log_group_name = /var/log/dmesg
log_stream_name = {cluster}/{container_instance_id}

[/var/log/messages]
file = /var/log/messages
log_group_name = /var/log/messages
log_stream_name = {cluster}/{container_instance_id}
datetime_format = %b %d %H:%M:%S

[/var/log/docker]
file = /var/log/docker
log_group_name = /var/log/docker
log_stream_name = {cluster}/{container_instance_id}
datetime_format = %Y-%m-%dT%H:%M:%S.%f

[/var/log/ecs/ecs-init.log]
file = /var/log/ecs/ecs-init.log.*
log_group_name = /var/log/ecs/ecs-init.log
log_stream_name = {cluster}/{container_instance_id}
datetime_format = %Y-%m-%dT%H:%M:%SZ

[/var/log/ecs/ecs-agent.log]
file = /var/log/ecs/ecs-agent.log.*
log_group_name = /var/log/ecs/ecs-agent.log
log_stream_name = {cluster}/{container_instance_id}
datetime_format = %Y-%m-%dT%H:%M:%SZ

[/var/log/ecs/audit.log]
file = /var/log/ecs/audit.log.*
log_group_name = /var/log/ecs/audit.log
log_stream_name = {cluster}/{container_instance_id}
datetime_format = %Y-%m-%dT%H:%M:%SZ
EOF

EC2_REGION=$(curl -s 169.254.169.254/latest/dynamic/instance-identity/document | jq -r .region)
sed -i -e "s/region = us-east-1/region = $EC2_REGION/g" /etc/awslogs/awscli.conf
--==BOUNDARY==
Content-Type: text/upstart-job; charset="us-ascii"
#upstart-job
description "Configure and start CloudWatch Logs agent on Amazon ECS container instance"
author "Amazon Web Services"
start on started ecs

script
  exec 2>>/var/log/ecs/cloudwatch-logs-start.log
  set -x

  until curl -s http://localhost:51678/v1/metadata
  do
    sleep 1
  done

  # Grab the cluster and container instance ARN from instance metadata
  CLUSTER=$(curl -s http://localhost:51678/v1/metadata | jq -r '. | .Cluster')
  CONTAINER_INSTANCE_ID=$(curl -s http://localhost:51678/v1/metadata | jq -r '. | .ContainerInstanceArn' | awk -F/ '{print $2}' )

  # Replace the cluster name and container instance ID placeholders with the actual values
  sed -i -e "s/{cluster}/$CLUSTER/g" /etc/awslogs/awslogs.conf
  sed -i -e "s/{container_instance_id}/$CONTAINER_INSTANCE_ID/g" /etc/awslogs/awslogs.conf

  service awslogs start
  chkconfig awslogs on
end script
--==BOUNDARY==--